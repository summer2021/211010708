# Seata Over Http2

## 项目名称

Seata 通信协议拓展支持HTTP2

## 方案描述

首先是框架的选择上，是使用 grpc 来进行实现。

简单的来说就是，通过封装来屏蔽掉 Rpc 模块的细节，使得上层应用对底层改变无感。

1. 在配置文件里增加 grpc 的选择
2. 将 GrpcServer 和 NettyServer 进行抽象并封装
3. 生成对应的 grpc 序列化类
4. 重写 Processor 类

## 已完成工作：

1. 使用 Protobuf 生成 grpc 相应的序列化类
2. 接入 grpc

## 遇到的问题

#### 使用Protoc生成的代码无法直接使用

protobuf 只是序列化协议，无法被 grpc 直接使用。

需要使用 protoc-gen-grpc-java 插件。

#### 思路上有点钻牛角尖

一开始我看到项目名字叫做，拓展支持HTTP2。

所以我一开始的思路是，使用原生Java，来写一个关于头部压缩，封装帧的算法。然后我就去看了一些相关的算法。

然后我看了Seata的源码，然后我又以为是使用Netty关于HTTP2的一些包，来实现收包，发包。然后我又去看了Netty关于HTTP2的源码，grpc是怎么使用Netty来实现HTTP2收包，发包。

后来才知道要用grpc来拓展支持HTTP2。然后我思路又有点钻牛角尖，一直想着怎么将grpc和rpc Netty模块融合在一起，比如将rpc Netty的一些Processor进行复用。后来发现其实应该把grpc模块和rpc Netty模块独立开来。

## 后续工作安排：

后续会对 Processor 类进行重载，即写关于 grpc 相关的 process 方法。 


开源供应链点亮计划OSPP Summer 2021
