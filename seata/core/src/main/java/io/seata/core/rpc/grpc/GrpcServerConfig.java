package io.seata.core.rpc.grpc;

/**
 * @author xilou31
 **/
public class GrpcServerConfig {
    private Integer port = 50051;

    public Integer getPort() {
        return port;
    }
}
