package io.seata.core.rpc.grpc.client;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.seata.core.protocol.grpc.SeataGrpc.BranchSession.BranchType;
import io.seata.core.rpc.grpc.client.service.BranchTransactionClient;

import java.nio.charset.StandardCharsets;

/**
 * @author xilou
 * @date 2021/8/21
 **/
public class GrpcClientBootstrap {
    protected GrpcClientConfig grpcClientConfig = new GrpcClientConfig();

    protected ManagedChannel channel = ManagedChannelBuilder.forAddress(grpcClientConfig.getServerHost()
            , grpcClientConfig.getServerPort())
            .usePlaintext()
            .build();

    public static void main(String[] args) {
        BranchTransactionClient client = new BranchTransactionClient();
        client.branchCommit("1", 1, "1", "1", BranchType.AT, ByteString.copyFrom("Hello World", StandardCharsets.UTF_8));
    }
}
