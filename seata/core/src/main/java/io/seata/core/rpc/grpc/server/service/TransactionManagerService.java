package io.seata.core.rpc.grpc.server.service;

import io.grpc.stub.StreamObserver;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalBeginRequest;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalBeginResponse;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalCommitRequest;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalCommitResponse;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalReportRequest;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalReportResponse;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalRollbackRequest;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalRollbackResponse;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalStatusRequest;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalStatusResponse;
import io.seata.core.protocol.grpc.TransactionManagerServiceGrpc;

/**
 * @author xilou
 * @date 2021/8/20
 **/

public class TransactionManagerService extends TransactionManagerServiceGrpc.TransactionManagerServiceImplBase {
    @Override
    public void begin(GlobalBeginRequest request, StreamObserver<GlobalBeginResponse> responseObserver) {
        GlobalBeginResponse response = GlobalBeginResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void commit(GlobalCommitRequest request, StreamObserver<GlobalCommitResponse> responseObserver) {
        GlobalCommitResponse response = GlobalCommitResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getStatus(GlobalStatusRequest request, StreamObserver<GlobalStatusResponse> responseObserver) {
        GlobalStatusResponse response = GlobalStatusResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void globalReport(GlobalReportRequest request, StreamObserver<GlobalReportResponse> responseObserver) {
        GlobalReportResponse response = GlobalReportResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void rollback(GlobalRollbackRequest request, StreamObserver<GlobalRollbackResponse> responseObserver) {
        GlobalRollbackResponse response = GlobalRollbackResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
