package io.seata.core.rpc.grpc.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.seata.core.rpc.grpc.server.service.BranchTransactionService;
import io.seata.core.rpc.grpc.server.service.ResourceManagerService;
import io.seata.core.rpc.grpc.server.service.TransactionManagerService;

/**
 * @author xilou
 * @date 2021/8/21
 **/
public class GrpcServerBootstrap {
    private static GrpcServerConfig grpcServerConfig = new GrpcServerConfig();

    public static void main(String[] args) {
        Server server = ServerBuilder.forPort(grpcServerConfig.getGrpcPort())
                .addService(new BranchTransactionService())
                .addService(new ResourceManagerService())
                .addService(new TransactionManagerService())
                .build();
        try {
            server.start();
            server.awaitTermination();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            server.shutdown();
        }
    }
}
