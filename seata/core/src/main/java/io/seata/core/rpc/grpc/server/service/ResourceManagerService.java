package io.seata.core.rpc.grpc.server.service;

import io.grpc.stub.StreamObserver;
import io.seata.core.protocol.grpc.ResourceManagerServiceGrpc;
import io.seata.core.protocol.grpc.SeataGrpc.BranchRegisterRequest;
import io.seata.core.protocol.grpc.SeataGrpc.BranchRegisterResponse;
import io.seata.core.protocol.grpc.SeataGrpc.BranchReportRequest;
import io.seata.core.protocol.grpc.SeataGrpc.BranchReportResponse;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalLockQueryRequest;
import io.seata.core.protocol.grpc.SeataGrpc.GlobalLockQueryResponse;

/**
 * @author xilou
 * @date 2021/8/20
 **/
public class ResourceManagerService extends ResourceManagerServiceGrpc.ResourceManagerServiceImplBase {
    @Override
    public void branchRegister(BranchRegisterRequest request, StreamObserver<BranchRegisterResponse> responseObserver) {
        BranchRegisterResponse response = BranchRegisterResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void branchReport(BranchReportRequest request, StreamObserver<BranchReportResponse> responseObserver) {
        BranchReportResponse response = BranchReportResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void lockQuery(GlobalLockQueryRequest request, StreamObserver<GlobalLockQueryResponse> responseObserver) {
        GlobalLockQueryResponse response = GlobalLockQueryResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
