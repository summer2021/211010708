package io.seata.core.rpc.grpc.client.service;

import io.seata.core.rpc.grpc.client.GrpcClientBootstrap;

/**
 * @author xilou
 * @date 2021/8/20
 **/
public class TransactionManagerClient extends GrpcClientBootstrap {
    public void begin() {

    }

    public void getStatus() {

    }

    public void globalReport() {

    }

    public void commit() {

    }

    public void rollback() {

    }
}
