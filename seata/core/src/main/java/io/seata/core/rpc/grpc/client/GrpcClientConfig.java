package io.seata.core.rpc.grpc.client;

import io.seata.core.rpc.grpc.server.GrpcServerConfig;

/**
 * @author xilou
 * @date 2021/8/21
 **/
public class GrpcClientConfig {
    GrpcServerConfig grpcServerConfig = new GrpcServerConfig();

    public String getServerHost() {
        return "localhost";
    }

    public int getServerPort() {
        return grpcServerConfig.getGrpcPort();
    }
}
