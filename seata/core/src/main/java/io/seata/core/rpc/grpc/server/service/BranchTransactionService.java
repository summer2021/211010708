package io.seata.core.rpc.grpc.server.service;


import io.grpc.stub.StreamObserver;
import io.seata.core.protocol.grpc.BranchTransactionServiceGrpc;
import io.seata.core.protocol.grpc.SeataGrpc.BranchCommitRequest;
import io.seata.core.protocol.grpc.SeataGrpc.BranchCommitResponse;
import io.seata.core.protocol.grpc.SeataGrpc.BranchRollbackRequest;
import io.seata.core.protocol.grpc.SeataGrpc.BranchRollbackResponse;

/**
 * @author xilou
 * @date 2021/8/20
 **/
public class BranchTransactionService extends BranchTransactionServiceGrpc.BranchTransactionServiceImplBase {
    @Override
    public void branchCommit(BranchCommitRequest request, StreamObserver<BranchCommitResponse> responseObserver) {
        System.out.println(request);
        BranchCommitResponse response = BranchCommitResponse.newBuilder()
                .setMessage("Hello World")
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void branchRollback(BranchRollbackRequest request, StreamObserver<BranchRollbackResponse> responseObserver) {
        BranchRollbackResponse response = BranchRollbackResponse.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
