package io.seata.core.rpc.grpc.client.service;

import com.google.protobuf.ByteString;
import io.seata.core.protocol.grpc.BranchTransactionServiceGrpc;
import io.seata.core.protocol.grpc.SeataGrpc.BranchCommitRequest;
import io.seata.core.protocol.grpc.SeataGrpc.BranchCommitResponse;
import io.seata.core.protocol.grpc.SeataGrpc.BranchSession;
import io.seata.core.rpc.grpc.client.GrpcClientBootstrap;

/**
 * @author xilou
 * @date 2021/8/20
 * <p>
 * message BranchCommitRequest {
 * string XID = 1;
 * int64 BranchID = 2;
 * string ResourceID = 3;
 * string LockKey = 4;
 * BranchSession.BranchType BranchType = 5;
 * bytes ApplicationData = 6;
 * }
 **/
public class BranchTransactionClient extends GrpcClientBootstrap {
    BranchTransactionServiceGrpc.BranchTransactionServiceBlockingStub stub;

    private BranchTransactionServiceGrpc.BranchTransactionServiceBlockingStub getStub() {
        if (stub == null) {
            stub = BranchTransactionServiceGrpc.newBlockingStub(channel);
            return stub;
        }
        return stub;
    }

    public void branchCommit(String xid, int branchId, String resourceId, String lockKey, BranchSession.BranchType branchType, ByteString applicationData) {
        BranchCommitRequest request = BranchCommitRequest.newBuilder()
                .setXID(xid)
                .setBranchID(branchId)
                .setResourceID(resourceId)
                .setLockKey(lockKey)
                .setBranchType(branchType)
                .setApplicationData(applicationData)
                .build();
        try {
            BranchCommitResponse response = getStub().branchCommit(request);
            System.out.println(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void branchRollback() {
    }
}
