package io.seata.core.rpc.grpc.server;

import io.seata.config.Configuration;
import io.seata.config.ConfigurationFactory;
import io.seata.core.constants.ConfigurationKeys;

import static io.seata.common.DefaultValues.DEFAULT_ENABLE_GRPC;
import static io.seata.common.DefaultValues.DEFAULT_GRPC_PORT;

/**
 * @author xilou
 * @date 2021/8/21
 **/
public class GrpcServerConfig {
    protected final Configuration CONFIG = ConfigurationFactory.getInstance();

    public boolean isEnableHttp2() {
        return CONFIG.getBoolean(ConfigurationKeys.ENABLE_GRPC,
                DEFAULT_ENABLE_GRPC);
    }

    public int getGrpcPort() {
        return CONFIG.getInt(ConfigurationKeys.SERVICE_GRPC_PORT,
                DEFAULT_GRPC_PORT);
    }
}
