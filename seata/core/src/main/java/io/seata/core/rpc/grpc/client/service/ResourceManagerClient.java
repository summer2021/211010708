package io.seata.core.rpc.grpc.client.service;

import io.seata.core.rpc.grpc.client.GrpcClientBootstrap;

/**
 * @author xilou
 * @date 2021/8/20
 **/
public class ResourceManagerClient extends GrpcClientBootstrap {
    public void branchRegister() {
    }

    public void branchReport() {

    }

    public void lockQuery() {

    }
}
